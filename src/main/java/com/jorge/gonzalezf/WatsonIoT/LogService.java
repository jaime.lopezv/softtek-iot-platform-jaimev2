package com.jorge.gonzalezf.WatsonIoT;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class LogService {

    private String message;
    private String status;
    private String service;

    public LogService(String message, String status, String service) {
        this.message = message;
        this.status = status;
        this.service = service;
    }

    public String executeAPI() {
        RestTemplate template = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("message", message);
        map.add("status", status);
        map.add("service", service);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
        String logService = "https://coe-logging.herokuapp.com/logTransaction";
        String result = template.postForObject(logService, request, String.class);
        return "Save transaction: " + result;
    }
}
