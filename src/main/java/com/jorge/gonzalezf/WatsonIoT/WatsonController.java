package com.jorge.gonzalezf.WatsonIoT;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@RestController
public class WatsonController {

    private final String USERNAME = "a-fgoz7e-9yuunwc991";
    private final String PASSWORD = "pREZH2442LY53sp??(";
    private final String URL = "https://fgoz7e.internetofthings.ibmcloud.com/api/v0002";

    @Autowired
    RestTemplate restTemplate;

    @CrossOrigin
    @RequestMapping("/iot/listDevices")
    public String listDevices() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth(USERNAME, PASSWORD);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        return restTemplate.exchange(
                URL + "/device/types/test1/devices",
                HttpMethod.GET, entity, String.class).getBody();
    }
    @CrossOrigin
    @RequestMapping("/iot/getStateDevice")
    public org.json.simple.JSONObject getStateDevice() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setBasicAuth(USERNAME, PASSWORD);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        String result = restTemplate.exchange(
                URL + "/device/types/Test1/devices/abc123/state/5ddebfdddd99360021f5d708",
                HttpMethod.GET, entity, String.class).getBody();

        JSONObject jsonObject;
        org.json.simple.JSONObject json = new org.json.simple.JSONObject();
        try {
            jsonObject = new JSONObject(result);
            int randomNumber = (int) jsonObject.getJSONObject("state").get("random");
            json.put("randomNumber", randomNumber);

            LogService logService = new LogService(json.toJSONString(), "200", "Watson IoT Platform");
            System.out.println(logService.executeAPI());
        } catch (JSONException e) {
            json.put("randomNumber", "500: " + e.getMessage());

            LogService logService = new LogService(e.getMessage(), "500", "Watson IoT Platform");
            System.out.println(logService.executeAPI());
        } finally {
            return json;
        }
    }
}
